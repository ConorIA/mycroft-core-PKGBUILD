# Maintainer: Matteo De Carlo <matteo.dek@covolunablu.org>

# Package heavily inspired from https://build.opensuse.org/package/show/home:alarrosa:packages/mycroft-core
# Many thanks for Antonio Larrosa for doing the hard work

pkgname=mycroft-core
pkgver=18.2.3
pkgrel=1
pkgdesc="Mycroft is a hackable open source voice assistant."
arch=('i686' 'x86_64')
url='https://mycroft.ai'
license=('GPLv3')

_pkgsrcname="${pkgname}-release-v${pkgver}"

depends=('mimic'
        'python-pychromecast'
        'python-pyric'
        'python-speechrecognition>=3.8.1'
        'python-adapt-parser'
        'python-fann2>=1.0.7'
        'python-future>=0.16.0'
        'python-google-api-python-client>=1.6.4'
        'python-gtts>=1.1.7'
        'python-gtts-token'
        'python-inflection'
        'python-monotonic'
        'python-dateutil'
        'python-padatious'
        'python-parsedatetime'
        'python-pycodestyle'
        'python-psutil'
        'python-pulse-control'
        'python-pyalsaaudio'
        'python-pyee>=1.0.1'
        'python-vlc'
        'python-requests-futures'
        'python-xmlrunner'
        'python-pip'
        'python-setuptools'
        'python-pillow'
        'python-pyserial'
        'python-pyaml'
        'python-six'
        'python-tornado'
        'python-virtualenv'
        'python-virtualenvwrapper'
        'python-websocket-client>=0.32.0'
        'python-gobject'
#--- Requirements of the default skills
        'python-pytz'
        'python-tzlocal'
        'python-astral>=1.4'
        'python-pyowm'
        'python-arrow'
        'python-duckduckgo2'
        'python-humanhash3'
        'python-requests'
        'python-feedparser'
        'python-pyjokes'
        'python-netifaces'
        'python-multi_key_dict'
        'python-wikipedia'
        'python-wolframalpha'
# Required by Plasma skills
        'python-dbus'
        'python-aiml'
        'python-xlib'
        'python-pyautogui'
        'python-num2words'
#----
        'glib2'
        'openssl'
        'libffi'
        'portaudio'
        'mpg123'
        'screen'
        'curl'
        'jq'
        'git'
        'patch'
        'libjpeg-turbo'
)

makedepends=(
        'fdupes'
       #'python-devel'
       #'python-pep8'
       #'python-pystache>=0.5.4'
        'swig'
       #'s3cmd'
        'icu'
)

optdepends=(
        'espeak'
        'plasma-mycroft'
        'vlc'
)
conflicts=()
install=mycroft-core.install
# changelog=ChangeLog
source=(
        "https://github.com/MycroftAI/${pkgname}/archive/release/v${pkgver}.tar.gz"

        # -- patches --
        0002-Make-speech-client-python3-compatible.patch
        0003-Make-text-client-python2-3-compatible.patch
        0004-Fix-hashes-in-settings-for-python3.patch
        0005-Fix-test-cases-under-python-3.patch
        0006-Remove-backwards-compatibility-with-python-2.7.patch
        0008-Fix-cli-crash-with-Python-3.patch
        0010-Use-sorted-json-to-perform-hash-of-settings.patch
        0013-Remove-backwards-compatibility-from-tests.patch
        fix-installation-paths.patch
        use-pycodestyle-instead-of-pep8.patch
        msm-use-python3.patch
        msm-add-local-patch-support.patch
        update-requirements.patch

        # -- skill patches --
        skills-fallback-wolfram-alpha.patch
        skills-skill-reminder.patch
        skills-mycroft-youtube.patch
        skills-mycroft-music-skill.patch
        skills-skill-desktop-launcher.patch
        skills-fallback-duckduckgo.patch
        skills-skill-autogui.patch

        # -- systemd files --
        create-initial-user-configuration.sh
        mycroft-user-template.conf
        mycroft.target
        mycroft-bus.service
        mycroft-audio.service
        mycroft-skills.service
        mycroft-voice.service

        # --
        version.json
        SKILL_README.md
        mycroft.conf
)

sha512sums=('e7c2e6337e13e9db69fa987b63134b8474989983b231ae368222827cf7b99860b8faa8fc30d7d0430e50f74ecabccc291792eab65b7025deead8e27965e598da'
            '1561e94b843398216e74f177a15bffb9b1cbf4b40bad0614a25221768d17bf2855d75f9d3be400b491f10d9a8b079a537a1379aa891bf6184da06ce7f465e7b0'
            'b70dd051b9565357dda060ed5c55831e374632cf37860d3ddaec6fc5f160b30469e8687032c5c604898ac5dbfe481bef36493e03cf46dd9384e0bb4c2c26e6b8'
            '65bd40209738152f7ac4c1b95a88b9810f6e86f30536067606ddda60cd16046d37055938fdaf8e493bf347df8e8848c7a99f89db3a376a7abea6050062129297'
            '3a9c8f349b6ba2206085561852b6882d152d9e5cfd80bed4be7fce54036084ff4e5b7bbe9c5adf59b514de99dbd97c87d14665c27031bc51b939cd00c22149f5'
            '06553b39e448183f006291ee7ffcc311a6b1fa722f26a74fd942a7ca6a03867ac3dc1e4c97bb6953b00f399159e340a86a40edf8e4ccc105051697d780306379'
            'b4a9bed5f63143b80929c752a05eb36428e253473fd44f0d1951b2a2242dd5d4cc05a89ee0eae6848bbe8ea2d13b27c9c03939f38c75dc715f825a2d3ebc8c9a'
            'df548c02b6bb1d709a244e6426a97c57888d837f8b71e0d1243a93b04b7309492420be86d9c192896e9c6e4d8f5cd26ac4a70aa0d751dff9843300d85e2ba40c'
            '8a9dec910484bdbc23fe4f188718bf9c3edf203fa095c047babe03045efe9f70a334bd67307150018b44090c1ee67a98f0b115ad967cf93fa96f17f41f19663e'
            '5f89dcdf0eb4d2ed961b3272ab46b17ae748c4ec9b20dea1c7b4c0a03d5bc323b4f2da6d33069d3cfd0411f36a1f49c47570c89fce1c0b0153eeb0e63f25c23b'
            '19f15bca8dca4f8152515e63e9d4e3c7751ec733941b35ff5d07a80b453899c0aa91787b0ce0ba6ffc2ece5f969b4c5a2566652f64a9c58df88d8d69a9a2efc8'
            '9f1afc5c0aeb9928d515d2a171aadf6039d9210c3229ea310b6ce825e3c318ec714b52ac7c80ab256eeff280d9fce1f40aa228ef71ab580c9b83d318e1aae749'
            '00901b006d934ddbeb54aeaff1500bd619973707fba9079b189ae12b9c0031547dc00497e5748d8c51e9b2406bf1806c65118e524c71792913b6b5eb0797f46d'
            'a5a75d3cce740ada1130df8f19079a08dc9c890a13bb4cca2e7e075e1564af5d29a35f4ee3ad6cc0b6f8b23dd36c5dd547fbd4f0514ed188ebf86c98fe16b9ea'
            'ce99ab2bb3e01045b6af04834b723a232d386b715a244e59aff92f939458e4edaf19e6ab52d80f3d46a412044be0df8316d9157652c133f913990e3a30cbc8ab'
            '46801752ac88b7f08f1314cfeb4e33836dfebeadbfa52e157af57037bbbd28e3bdd016fafb079090e725d5799e82f3521c59904f8c8f01b447ffa83e59afd40e'
            '829703d1d37d997c987e05b9505ba805151b83866a0059ca1971e204421a441c92c08bb42116b070768bba06a5f0b837c01e4ca556ef3d0e53eae5e66dbb6809'
            '8fe17272b5a28380288ec0c256746e577fbf7cc1ce839a41b45da3b1e7d311b105b687fe60decfe168e277725b85a154129f430a2a892a6c726617e99f30d22c'
            '1b7607a3d84007857dbbec60ddf4541fce23bc6cebecdd5a2bce164a13ebe40d35533509a3d53b62cab4e2cf9a05ff4ba831cb1cd3dd89331834c177793f1014'
            '5ea098d6791ab4dba0f8f14ff842e60c729c7b543a760c4630c2d4c4b7ac254872b2699ed94f4e4503607b872eb276a5154482103a63566d6a33e0a0ad24993e'
            '2f90316a8b21860f1aa274c7c174dae5f79bf98b41c385049e202d5be645f0bf76547110cc124644ccea7c261991be3696029ff2eadb509207cce289db80ce70'
            '21560635ae012e5b35cecc930ef602295c3423ecaeb519b5172cbcb1d33cfa7413c2b5a48a4bfeb05a811d239ce094774f73206792c2268460982518ae98d516'
            '57136cfa8de1957b73d0056302beaa2fc35a44629bd189a5070ca538b62527af49e1aaa69328a720d98cc66242306967a4b186bc86f9fe77025ddabbd6c59d3e'
            '57928da4a6e7e54857bf1f1d91beed5b52ab5e44929c6954b76ee1a90aac189febd1d9f052f396c51d15352441635320baab63ca32217f8012188cffb60ad607'
            '966a8ff173c7afabcb0cc23dd6288d77c7ec7c9072456795c5a586543a412fd5de45c745bdb532b2e40253fa41f2a8297abdc7ee5c01a89aa2701226f2c5a248'
            'a719e89a6f2c2e4633adffb8d30981f6cc8a691df4b9f581212eb57b099d62668d86c0d3554c87b3ebed84ca767106ac2774582eaf9f6771f3e77344237d499d'
            '7bf833e08e3eead0cfd3378ebab4e0d6f741b5e4bbe2d05d396d8658dd01964f390a4735cf1589f3cc936e65e531f798dc45c6942408a5908434f57bb5e22199'
            'd7f77406f04a2fb9d519a33326326e4769a5b689271246e8bb6596de88c9f645f94f72819242393dc6d0d2c303dae037e434f8455e8e9bf0e97a0736a627905a'
            '272ff07f891b9399a01c4e0c30a8e72e2563a4f685449302fde0fa71b35a836709e863c08d007470203113df20c825796ef10bec1f214b82468ee05da3b9b63a'
            '759d34b85dca85face95c08d160c8d2edcebae5392d5a837a50d56b4b973c7932130b31ae00835d681dcb53d71ff34730af99323ab5f205b13930d5b8c7b34fd'
            '901883f35d70b40f32069575997544fbce4d2a7a5f5c110e221e1e2c94f162043997a6b3044098f6db5d823428964aada7f47dfeb61e6444b4bcb94257211493')

prepare() {
  cd $srcdir/$_pkgsrcname
  patch -p1 -i ../0002-Make-speech-client-python3-compatible.patch
  patch -p1 -i ../0003-Make-text-client-python2-3-compatible.patch
  patch -p1 -i ../0004-Fix-hashes-in-settings-for-python3.patch
  patch -p1 -i ../0005-Fix-test-cases-under-python-3.patch
  patch -p1 -i ../0006-Remove-backwards-compatibility-with-python-2.7.patch
  patch -p1 -i ../0008-Fix-cli-crash-with-Python-3.patch
  #patch -p0 -i ../0010-Use-sorted-json-to-perform-hash-of-settings.patch
  patch -p1 -i ../0013-Remove-backwards-compatibility-from-tests.patch
  patch -p1 -i ../fix-installation-paths.patch
  patch -p1 -i ../use-pycodestyle-instead-of-pep8.patch
  patch -p0 -i ../msm-use-python3.patch
  patch -p0 -i ../msm-add-local-patch-support.patch
  patch -p1 -i ../update-requirements.patch
}


build() {
  cd $srcdir/$_pkgsrcname
  python3 mycroft-base-setup.py build
}

package() {
  cd $srcdir/$_pkgsrcname
  python3 mycroft-base-setup.py install --prefix=/usr --root="${pkgdir}"
  install -D -m 0755 "${srcdir}/${_pkgsrcname}/msm/msm" "${pkgdir}/usr/bin/msm"

  mkdir -p "${srcdir}/usr/share/mycroft-core/skills"

  install -D -m644 "${srcdir}/version.json" "${pkgdir}/usr/share/mycroft-core/version.json"

  # Symtemd units
  install -d "${pkgdir}/usr/lib/systemd/user/"
  install -D -m644 "${srcdir}/mycroft.target"         "${pkgdir}/usr/lib/systemd/user/mycroft.target"
  install -D -m644 "${srcdir}/mycroft-bus.service"    "${pkgdir}/usr/lib/systemd/user/mycroft-bus.service"
  install -D -m644 "${srcdir}/mycroft-audio.service"  "${pkgdir}/usr/lib/systemd/user/mycroft-audio.service"
  install -D -m644 "${srcdir}/mycroft-skills.service" "${pkgdir}/usr/lib/systemd/user/mycroft-skills.service"
  install -D -m644 "${srcdir}/mycroft-voice.service"  "${pkgdir}/usr/lib/systemd/user/mycroft-voice.service"

  install -D -m644 "${srcdir}/SKILL_README.md" "${pkgdir}/usr/share/doc/mycroft-core/SKILL_README.md"
  install -D -m644 "${srcdir}/${_pkgsrcname}/mycroft/configuration/mycroft.conf" "${pkgdir}/usr/share/doc/mycroft-core/mycroft.conf"
  install -D -m644 "${srcdir}/mycroft.conf" "${pkgdir}/etc/mycroft/mycroft.conf"
  install -D -m644 "${srcdir}/mycroft-user-template.conf" "${pkgdir}/usr/share/mycroft-core/mycroft-user-template.conf"
  install -D -m755 "${srcdir}/create-initial-user-configuration.sh"    "${pkgdir}/usr/share/mycroft-core/create-initial-user-configuration.sh"
  install -D -m755 "${srcdir}/${_pkgsrcname}/skiller.sh"    "${pkgdir}/usr/share/mycroft-core/skiller.sh"

  install -D -m644 "${srcdir}/skills-fallback-wolfram-alpha.patch" "${pkgdir}/usr/share/mycroft-core/skill-patches/fallback-wolfram-alpha.patch"
  install -D -m644 "${srcdir}/skills-skill-reminder.patch"         "${pkgdir}/usr/share/mycroft-core/skill-patches/skill-reminder.patch"
  install -D -m644 "${srcdir}/skills-mycroft-youtube.patch"        "${pkgdir}/usr/share/mycroft-core/skill-patches/mycroft-youtube.patch"
  install -D -m644 "${srcdir}/skills-mycroft-music-skill.patch"    "${pkgdir}/usr/share/mycroft-core/skill-patches/mycroft-music-skill.patch"
  install -D -m644 "${srcdir}/skills-skill-desktop-launcher.patch" "${pkgdir}/usr/share/mycroft-core/skill-patches/skill-desktop-launcher.patch"
  install -D -m644 "${srcdir}/skills-fallback-duckduckgo.patch"    "${pkgdir}/usr/share/mycroft-core/skill-patches/fallback-duckduckgo.patch"
  install -D -m644 "${srcdir}/skills-skill-autogui.patch"          "${pkgdir}/usr/share/mycroft-core/skill-patches/skill-autogui.patch"
}
